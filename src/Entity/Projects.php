<?php

namespace App\Entity;

use App\Repository\ProjectsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProjectsRepository::class)]
class Projects
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 45)]
    private $title;

    #[ORM\Column(type: 'string', length: 800)]
    private $description;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $imgUrl1;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $imgUrl2;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $urlRepo;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $urlSite;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImgUrl1(): ?string
    {
        return $this->imgUrl1;
    }

    public function setImgUrl1(?string $imgUrl1): self
    {
        $this->imgUrl1 = $imgUrl1;

        return $this;
    }

    public function getImgUrl2(): ?string
    {
        return $this->imgUrl2;
    }

    public function setImgUrl2(?string $imgUrl2): self
    {
        $this->imgUrl2 = $imgUrl2;

        return $this;
    }

    public function getUrlRepo(): ?string
    {
        return $this->urlRepo;
    }

    public function setUrlRepo(?string $urlRepo): self
    {
        $this->urlRepo = $urlRepo;

        return $this;
    }

    public function getUrlSite(): ?string
    {
        return $this->urlSite;
    }

    public function setUrlSite(?string $urlSite): self
    {
        $this->urlSite = $urlSite;

        return $this;
    }
}
