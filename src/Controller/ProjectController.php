<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Projects;
use App\Form\ProjectType;

class ProjectController extends AbstractController
{
    #[Route('/', name: 'app_project')]
    public function index(): Response
    {
        return $this->render('project/index.html.twig', [
            'controller_name' => 'Home',
        ]);
    }

    #[Route('/portfolio', name: 'portfolio')]
    public function portfolio(ManagerRegistry $doctrine): Response
    {
        $projects = $doctrine->getRepository(Projects::class)->findAll();

        return $this->render('project/portfolio.html.twig', [
            'controller_name' => 'portfolio',
            'data' => $projects,
        ]);
    }

    #[Route('/add-project', name: 'addproject')]
    public function addproject(ManagerRegistry $doctrine, Request $request, EntityManagerInterface $entityManager):Response
    {
        $project = new Projects();

        $form = $this->createForm(ProjectType::class, $project);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $entityManager->persist($project);
            $entityManager->flush();
        }

        return $this->render('project/add-project.html.twig', [
            'form' => $form->createView(),
        ]);

    }
}
